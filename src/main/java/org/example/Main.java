package org.example;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[8];
        for (int i = 0; i < array.length; i++){
            array[i] = (int) (Math.random() * 10) + 1;
        }
        System.out.println(Arrays.toString(array));
        boolean subsequence = true;

        for (int i = 1; i < array.length; i++){

            if(array[i] <= array[i-1]) {
                subsequence = false;
                break;
            }

        }
        System.out.println(subsequence ? "Массив является послед. возрастающим" : "Массив не является послед. возрастающим");

        for (int i = 0; i < array.length; i++){

            if (i % 2 == 0){
                array[i] = 0;
            }
        }
        System.out.println(Arrays.toString(array));
    }
}